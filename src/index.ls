'use strict'

require! {
	$: \jquery
	_: \lodash
	Promise: \bluebird
}

module.exports = $ -> 
	console.log 'APP  start'
	console.log 'jQuery Loaded', true
	console.log 'Lodash Loaded', _.identity true
	Promise.resolve true .then -> console.log 'Bluebird Loaded', true