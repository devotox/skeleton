Build Skeleton

### Commands -
	`gulp - Runs all
	gulp dev - Runs all and watches
	gulp watch - Watches all folders
	gulp test - Runs all lints / tests
	gulp prod - Runs all and cleans dev
	gulp templates - Compiles HTML ['html', 'jade', 'dust']
	gulp images - Compiles Images - ['jpg', 'png', 'gif', 'bmp']
	gulp styles - Compiles Styles - ['css', 'less', 'sass', 'stylus']
	gulp scripts - Compiles Scripts - ['javascript', 'livescript', 'coffeescript', 'typescript']
	gulp publish - Updates package.json & bower.json creates release, creates git tag, and publishes to npm & bower`


### Caveats -
	`Files / Folders are retrieved in alphabetical order`

### Defaults -
	`var paths = {
	  manifest: ['build/**/*'],
	  images: ['res/images/**/*'],
	  styles: ['res/styles/**/*'],
	  html: ['res/html/**/*.html'],
	  scripts: ['res/libs/**/*', 'res/scripts/**/*'],
	  dust: ['res/templates/**/*.dust', 'res/html/**/*.dust'],
	  jade: ['res/templates/**/*.jade', 'res/html/**/*.jade'],

	  css: ['res/styles/**/*.css'],
	  less: ['res/styles/**/*.less'],
	  sass: ['res/styles/**/*.scss'],
	  stylus: ['res/styles/**/*.styl'],

	  javascript: ['res/libs/**/*.js', 'res/scripts/**/*.js'],
	  livescript: ['res/libs/**/*.ls', 'res/scripts/**/*.ls'],
	  typescript: ['res/libs/**/*.ts', 'res/scripts/**/*.ts'],
	  coffeescript: ['res/libs/**/*.coffee', 'res/scripts/**/*.coffee'],

	  jshint: {
	    jshintrc: '.jshintrc'
	  },

	  test: {
	    action: 'run',
	    all: ['test/*.js'],
	    karmarc: ".karmarc",
	    mocharc: ".mocharc",
	    mocha: ['test/*.mocha.js'],
	    karma: ['test/*.karma.js'],
	    jasmine: ['test/*.jasmine.js']
	  },

	  publish: {
	    root: './',
	    bumpType: 'patch',
	    packages: ['./package.json', './bower.json']    
	  },

	  build: {
	    maps: '../maps',
	    prod_folder: ['build'],
	    dev_folder: ['dev-build'],
	    folders: {
	      images: 'build/images',
	      styles: 'build/styles',
	      scripts: 'build/scripts',
	        templates: 'build/templates'
	    },
	    filenames: {
	      manifest: [ pkg.name, 'manifest' ].join('.'),
	      styles: [ pkg.name, 'styles.css' ].join('.'),
	      scripts: [ pkg.name, 'scripts.js' ].join('.'),
	      min: {
	        styles: [ pkg.name, 'styles.min.css' ].join('.'),
	        scripts: [ pkg.name, 'scripts.min.js' ].join('.')
	      }
	    },
	    dev: {
	      folders: {
	        images: 'dev-build/images',
	        styles: 'dev-build/styles',
	        scripts: 'dev-build/scripts',
	        templates: 'dev-build/templates'
	      },
	      glob: {
	        styles: ['dev-build/images/**/*'],
	        styles: ['dev-build/styles/**/*.css'],
	        scripts: ['dev-build/scripts/**/*.js']
	      }
	    }
	  }
	};`

