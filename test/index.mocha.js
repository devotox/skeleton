
var should = require('should');
var expect = require('expect.js');

describe("Test Fail", function() {
	beforeEach(function() { });
	afterEach(function() { });

	it('should fail', function(done) {
		expect(true).to.equal(false); 
		done();
	});
});

describe('Test Pass', function () {
	beforeEach(function() { });
	afterEach(function() { });

	it('should pass', function (done) {
		expect(1 + 1).to.equal(2);
		done();
 });
});